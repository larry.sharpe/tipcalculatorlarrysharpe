import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import TipCalculator from "@/components/TipCalculator.vue";

const localVue = createLocalVue()

describe("TipCalculator.vue", () => {

  const mountFunction = (options, shallow) => {
    return (shallow ? shallowMount : mount)(TipCalculator, {
      localVue,
      ...options
    })
  }


  it('it properly renders TipCalculator', async () => {
    const ELE = mountFunction({})
    expect(ELE.html()).toMatchSnapshot()
  })



});
