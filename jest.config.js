module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  setupFiles: ['./jest.setup.js'],
  setupFilesAfterEnv: ['./jest.setupAfterEnv.js'],

  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{js,vue}',
    '!**/node_modules/**',
    '!src/plugins/*'
  ],
  transformIgnorePatterns: ['/node_modules/(?!lodash-es)']
}
